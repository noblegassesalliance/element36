#include "../include/drive.hpp"
#include "../include/sensors.hpp"
#include "cmath"
#include "../include/joystick.hpp"

namespace drive {
    bool driveConstant = true;
    motors frontLeft;
    motors frontRight;
    motors backLeft;
    motors backRight;

    void init(int motorNum, bool isReverse, bool isTruespeed, char location[]) {
        if(location == "frontLeft") {
            frontLeft.motorNum = motorNum;
            frontLeft.isReverse = isReverse;
            frontLeft.isTruespeed = isTruespeed;
        } else if(location == "frontRight") {
            frontRight.motorNum = motorNum;
            frontRight.isReverse = isReverse;
            frontRight.isTruespeed = isTruespeed;
        } else if(location == "backRight") {
            backRight.motorNum = motorNum;
            backRight.isReverse = isReverse;
            backRight.isTruespeed = isTruespeed;
        } else if(location == "backLeft") {
            backLeft.motorNum = motorNum;
            backLeft.isReverse = isReverse;
            backLeft.isTruespeed = isTruespeed;
        }
    }

    //truespeed mappingns
    int MC29[ 128 ] = {0,0,0,0,0,0,0,0,0,0,0,10,10,10,10,11,11,11,11,12,12,12,12,13,13,13,13,13,14,14,14,14,15,15,15,15,15,16,16,16,16,17,17,17,17,17,18,18,18,18,18,19,19,19,20,20,20,20,21,22,22,22,22,23,23,23,24,24,24,24,25,25,25,26,26,27,27,27,28,28,28,29,29,31,32,32,32,32,32,33,33,34,35,35,36,36,36,38,38,39,39,41,42,43,43,46,46,47,47,48,51,52,54,55,57,58,63,64,67,69,73,75,81,83,84,84,87,127,};  

    int sgn(int x) {
        if(x > 127) {
            return(127);
        }
        else return(x);
    }

    using namespace std;
    pid sDrivePID;
    int
    iDrivePID( int target ) {
    sDrivePID.kP = .2;
    sDrivePID.kD = 1;
    sDrivePID.current = sensors::drive::get();
    sDrivePID.error = target - sDrivePID.current;
    sDrivePID.derivative = sDrivePID.error - sDrivePID.lastError;
    sDrivePID.lastError = sDrivePID.error;
    return ( (sDrivePID.error * sDrivePID.kP) + (sDrivePID.derivative *sDrivePID.kD) + (15 * (sDrivePID.error / abs(sDrivePID.error) ) ) );
    }

    pid sRotatePID;
    int iRotatePID( int target) {
    sRotatePID.kP = 9;
    sRotatePID.kD = 6;
    sRotatePID.kI = 0; 
    sRotatePID.current = sensors::gyro::get();
    sRotatePID.error = target - sRotatePID.current;
    sRotatePID.integral += sRotatePID.error;  
    sRotatePID.derivative = sRotatePID.error - sRotatePID.lastError;
    sRotatePID.lastError = sRotatePID.error;
    return ( (sRotatePID.error * sRotatePID.kP)+ (sRotatePID.derivative * sRotatePID.kD) + (sRotatePID.integral * sRotatePID.kI) + 15 *(sRotatePID.error/(abs(sRotatePID.error))));
    }


  void vDrive(void*parameter) {
      while(isAutonomous()) {
          speed(iDrivePID(((int)parameter)));
      }
  }

  void vRotate(void*parameter) {
      while(isAutonomous()) {
          right(iRotatePID(((int)parameter)));
          left(-iRotatePID(((int)parameter)));
      }
  }


    void calculateTrueSpeed() {
        float speed[128];
        int dist_x;
        int dist_y;
        int i;
        for(i = 0; i < 128; i++) {
            trueSpeed(9, i);
            wait(250);
            dist_x   = sensors::drive::get();
            delay(1000);
            dist_y   = sensors::drive::get();
            speed[i] = ((dist_y - dist_x) / 6);
            printf("Speed output at speed %d is %.2f , Time remaining: %.2f seconds\n", i, speed[i], ((127-i)*1.25));
        }
        printf("Printing original array:\n");
        delay(500);
        for(i = 0; i < 128; i++) {
            printf("%.1f, ", speed[i]);
        }
        
         /*
        double maxVal = speed[127];
        int maxVal_t = 127;
        double minVal = 100;
        int minVal_t;
        for(i = 0; i < 128; i++) {
            if(speed[i] != 0) {
                if(speed[i] < minVal) {
                    speed[i] = minVal;
                    minVal_t = i;
                    printf("New minimum found!\n");
                }
            }
        }
        float slope = (maxVal-minVal)/(maxVal_t-10);   
        printf("Regregating...\n");
        float trueSpeed_a[128];
        for(i = 0; i < 128; i++) {
            trueSpeed_a[i] = (minVal + (slope)*(i));
        }
        printf("Printing finalized array:\n");
        delay(500);
        for(i = 0; i < 128; i++) {
            printf("%.2f, ", trueSpeed_a[i]);
        } */

    }

    void trueSpeed(int iPort, int iSpeed) {
        if(iSpeed > 0) {
            motorSet(iPort, MC29[sgn(abs(iSpeed))]);
        }
        else if(iSpeed < 0) {
            motorSet(iPort, -(MC29[sgn(abs(iSpeed))]));
        }
        else {
            motorSet(iPort, 0);
        }
    }

    void left(int iSpeed) {
        motorSet(frontLeft.motorNum, iSpeed);
        motorSet(backLeft.motorNum, -iSpeed);
    }

    void right(int iSpeed) {
        motorSet(frontRight.motorNum, -iSpeed);
        motorSet(backRight.motorNum, -iSpeed);
    }

    void speed(int iSpeed) {
        left(iSpeed);
        right(iSpeed);
    }
    void timed(int iSpeed, int duration) {
        speed(iSpeed);
        delay(duration);
        speed(0);
    } 
    
    /*
    void waitUntil(int iSpeed, int target) {
        speed(iSpeed);
        if(sensor::get(enc) > target) {
            while(sensor::get(enc) > target) {
                delay(15);
            }
        }
        else if(sensor::get(enc) < taget) {
            while(sensor::get(enc) < target) {
                delay(15);
            }
        }
    }   */
    
    unsigned long cycleTime;
    namespace teleop {
        void arcade(int speed, int turn) {
            if(abs(speed) < 20) speed = 0;
            if(abs(turn) < 20) turn = 0;

             speed = !driveConstant ? (speed + 9) : speed;
             left(speed + turn);
             right(speed - turn);
             if(joystick::digital(8, joystick::Right) && (millis() - cycleTime) > 250) {
             driveConstant = !driveConstant;
             cycleTime = millis();
            } 
        }
    }
}