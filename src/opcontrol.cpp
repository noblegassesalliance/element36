#include "../include/main.h"

void opCtrl() {
  drive::teleop::arcade(joystick::analog(2) + joystick::analog(3), joystick::analog(1) + joystick::analog(4));
  arm::teleop();
  mogo::teleop();
  rollers::teleop();
  bar::teleop(); 
  /*printf("arm %d mogo %d drive %d bar %d gyro %d fwd %d hor %d\n", sensors::arm::get(), sensors::mogo::get(), sensors::drive::get(), sensors::bar::get(),
                                                                    sensors::gyro::get(), joystick::analog(2), joystick::analog(1)); */

  printf("%d %d %d\n", sensors::arm::get(), sensors::bar::get(), (motorGet(6)/abs(motorGet(6))));
  //lcdPrint(uart2, 1, "B: %d, M: %d", sensors::bar::get(), sensors::mogo::get());
  //lcdPrint(uart1, 2, "B: %d G: %d", sensors::bar::get(), sensors::gyro::main::get());
  if(joystick::digital(8, joystick::Left)) autonomous();
  delay(15);
}

void operatorControl() {
  lcdSetBacklight(uart2, false); 
  while(true) {
    opCtrl();
  }  
}