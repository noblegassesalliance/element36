#include "API.h"

namespace sensors {
    namespace drive {
        int get();
        void reset();
    }
    namespace gyro {
        int get();
        void reset();
    }

    namespace arm {
        int get();
    }
    
    namespace mogo {
        int get();
    }

    namespace bar {
        int get();
    }
}