#include "../include/rollers.hpp"
#include "../include/joystick.hpp"
#include "../include/sensors.hpp"

namespace rollers {
    motors main;

    void init(int motorNum, bool isReverse) {
        main.motorNum = motorNum;
        main.isReverse = isReverse;
    }

    void speed(int iSpeed) {
        motorSet(main.motorNum, iSpeed * main.isReverse);
    } 
    
    void timed(int iSpeed, int duration) {
        speed(iSpeed);
        delay(duration);
        speed(0);
    } 

    int iOutput; int startTime; bool toggled;
    void teleop() {
       
        if(joystick::digital(8, joystick::Up)) {
            iOutput = -127;
            toggled = false;
        }
        else if(joystick::digital(8,joystick::Down)) {
            iOutput = 127;
            toggled = true;
            startTime = millis();
        }
        else iOutput = -20;

        if(toggled && (millis() - startTime < 250)) iOutput = 127;
        speed(iOutput);
    }
}