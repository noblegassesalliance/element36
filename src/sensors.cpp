#include "../include/sensors.hpp"

Gyro gyroMain;
Encoder encLeft;

namespace sensors {

    namespace drive {
        bool encL_init = false;
        int get() {
            if(!encL_init) {
                encLeft = encoderInit(11, 12, true);
                encL_init = true;
            }
            return(encoderGet(encLeft)); 
            }
        void reset() {
            encoderReset(encLeft);
        }
    }
    namespace gyro {
        bool mainInit = false;
        int get() {
            if(!mainInit) {
                gyroMain = gyroInit(4, 0);
                mainInit = true;
            }
            return(gyroGet(gyroMain));
        }
        void reset() {
            gyroReset(gyroMain);
        }
    }

    namespace arm {
        int armSensor = 3;
        int get() {
            return(analogRead(armSensor));
        }
    }
    
    namespace mogo {
        int mogoSensor = 1;
        int get() {
            return(analogRead(mogoSensor));
        }
    }

    namespace bar {
        int barSensor = 2;
        int get() {
            return(analogRead(barSensor));
        }
    }
}