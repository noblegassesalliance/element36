#include "../include/main.h"
#include "../include/lcd.hpp"

extern "C" {
void __libc_init_array();
}

void initializeIO() {
  __libc_init_array();
  rollers::init(6, -1);
  arm::init(8, 1, "left");
  arm::init(3, 1, "right");
  drive::init(9, 1, true, "frontLeft");
  drive::init(10, 1, true, "backLeft");
  drive::init(1, 1, true, "backRight");
  drive::init(2, -1, true, "frontRight");
  mogo::init(5, -1, "left");
  mogo::init(7, 1, "right");
  bar::init(4, 1, "main");
} 

void initialize() {
  lcdInit(uart1);

  lcdAuton_Init();
  if(!isEnabled()) {
    lcdAuton();
  }
}
