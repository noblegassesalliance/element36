#include "../include/bar.hpp"
#include "../include/sensors.hpp"
#include "../include/joystick.hpp"

namespace bar {
    motors main;

    void init(int motorNum, bool isReverse, char location[]) {
        if(location == "main") {
            main.motorNum = motorNum;
            main.isReverse = isReverse;
        }
    }

    void speed(int iSpeed) {
        motorSet(main.motorNum, iSpeed);
    } 
    void timed(int iSpeed, int duration) {
        speed(iSpeed);
        delay(duration);
        speed(0);
    } 
    /*
    void waitUntil(int iSpeed, int target) {
        speed(iSpeed);
        if(sensors::get(arm) > target) {
            while(sensors::get(arm) > target) {
                delay(15);
            }
        }
        if(sensors::get(arm) < target) {
            while(sensors::get(arm) < target) {
                delay(15);
            }
        }   
    }*/

    int iOutput, ratioVal;
    void teleop() {
        if(joystick::digital(6, joystick::Up) && sensors::bar::get() > 1040) iOutput = 127;
        else if(joystick::digital(6, joystick::Down) && sensors::bar::get() < 2690) iOutput = -127;
        else iOutput = 0;
        
        if(iOutput > 127) iOutput = 127;
        if(iOutput < -127) iOutput = -127;

        speed(iOutput);
    }
}