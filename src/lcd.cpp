#include "../include/lcd.hpp"

int selectAuton[4];
void lcdAuton_Pages(int selectVal)
{
    if (selectVal == 0)
    {
        lcdSetText(uart1, 1, " Swipe right >> ");
        lcdSetText(uart1, 2, "[None]    [PICK]");
    }
    if (selectVal == 1)
    {
        lcdSetText(uart1, 1, "      Mogo      ");
        lcdSetText(uart1, 2, "    [Select]    ");
    }
    if (selectVal == 2)
    {
        lcdSetText(uart1, 1, "   Stationary   ");
        lcdSetText(uart1, 2, "    [Select]    ");
    }
    if (selectVal == 3) 
    {
        lcdSetText(uart1, 1, "     Preload    ");
        lcdSetText(uart1, 2, "    [Select]    ");

    }
    if (selectVal == 4)
    {
        lcdSetText(uart1, 1, "   Prog Skill   ");
        lcdSetText(uart1, 2, "    [Select]    ");
    }
}

int initSensor = 0;
void lcdAuton_Init()
{
    lcdSetText(uart1, 1, "Initializing....");
    lcdSetText(uart1, 2, "[              ]");
    wait(100);
    lcdSetText(uart1, 2, "[=             ]");
    wait(100);
    lcdSetText(uart1, 2, "[==            ]");
    wait(100);
    lcdSetText(uart1, 2, "[===           ]");
    initSensor = sensors::gyro::get();
    wait(100);
    lcdSetText(uart1, 2, "[====          ]");
    wait(100);
    lcdSetText(uart1, 2, "[=====         ]");
    wait(100);
    lcdSetText(uart1, 2, "[======        ]");
    wait(100);
    lcdSetText(uart1, 2, "[=======       ]");
    wait(100);
    lcdSetText(uart1, 2, "[========      ]");
    wait(100);
    lcdSetText(uart1, 2, "[=========     ]");
    wait(100);
    lcdSetText(uart1, 2, "[==========    ]");
    wait(100);
    initSensor = sensors::drive::get();
    lcdSetText(uart1, 2, "[==========    ]");
    wait(100);
    lcdSetText(uart1, 2, "[===========   ]");
    wait(100);
    lcdSetText(uart1, 2, "[============  ]");
    wait(100);
    lcdSetText(uart1, 2, "[============= ]");
    wait(100);
    lcdSetText(uart1, 2, "[==============]");
    wait(250);
}

void lcdAuton()
{
    int lcdAutonPage = 0;
    while (true)
    {
        lcdAuton_Pages(lcdAutonPage);
        if (lcdReadButtons(uart1) == 4)
            lcdAutonPage += 1;
        if (lcdAutonPage > 0)
        {
            lcdSetBacklight(uart1, true);
            if (lcdReadButtons(uart1) == 1)
                lcdAutonPage -= 1;
        }
        if (lcdReadButtons(uart1) == 2)
        {
            break;
        }
        if (lcdReadButtons(uart1) > 0)
        {
            delay(500);
        }
        delay(10);
    }
    selectAuton[0] = lcdAutonPage;
    lcdAutonPage = 1;
    if (selectAuton[0] == 1 || selectAuton[0] == 2 || selectAuton[0] == 4 || selectAuton[0] == 3)
    {
        if (selectAuton[0] == 1 || selectAuton[0] == 3)
        {
            delay(250);
            lcdPrint(uart1, 2, "[-]          [+]", lcdAutonPage);
            while (true)
            {
                lcdPrint(uart1, 1, "    %d cones    ", lcdAutonPage);
                if (lcdReadButtons(uart1) == 1)
                {
                    lcdAutonPage--;
                }
                else if (lcdReadButtons(uart1) == 4)
                {
                    lcdAutonPage++;
                }
                else if(lcdReadButtons(uart1) == 2) {
                    break;
                    wait(100);
                }
                
                if(lcdReadButtons(uart1) > 0) {
                    delay(150);
                }
                delay(10);
            }
            selectAuton[1] = lcdAutonPage;
            lcdSetText(uart1, 1, "   What side?   ");
            lcdSetText(uart1, 2, "[R]          [B]");
            wait(250);
            while (true)
            {
                if (lcdReadButtons(uart1) == 1)
                {
                    lcdAutonPage = 1;
                    break;
                }
                else if (lcdReadButtons(uart1) == 4)
                {
                    lcdAutonPage = 2;
                    break;
                }
                delay(10);
            }
            selectAuton[2] = lcdAutonPage;
            lcdAutonPage = 0;
            lcdSetText(uart1, 1, "   What zone?   ");
            lcdSetText(uart1, 2, "[5]         [20]");
            wait(250);
            while (true)
            {
                if (lcdReadButtons(uart1) == 1)
                {
                    lcdAutonPage = 1;
                    break;
                }
                else if (lcdReadButtons(uart1) == 4)
                {
                    lcdAutonPage = 3;
                    break;
                }
                delay(10);
            }
            delay(100);
            selectAuton[3] = lcdAutonPage;
        }
        if (selectAuton[0] == 3)
        {
            lcdSetText(uart1, 1, "      GTFO      ");
            lcdSetText(uart1, 2, "[L]   [NO]   [R]");
            wait(250);
            while (true)
            {
                if (lcdReadButtons(uart1) == 1)
                {
                    lcdAutonPage = 1;
                    break;
                }
                if (lcdReadButtons(uart1) == 2)
                {
                    lcdAutonPage = 2;
                    break;
                }
                if (lcdReadButtons(uart1) == 4)
                {
                    lcdAutonPage = 3;
                    break;
                }
                delay(10);
            }
            selectAuton[1] = lcdAutonPage;
            selectAuton[2] = 0;
            selectAuton[3] = 0;
        }
    }

    lcdClear(uart1);
    lcdSetBacklight(uart1, false);
}

void lcdMain(void *parameter)
{

}